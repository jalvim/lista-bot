# Bot de automatização de lista de compras

Bot para o aplicativo Telegram criado para fins didáticos e práticos do dia-a-dia, com o intuito de agilizar o processo de escrita e consulta de lista de compras. A aplicação foi toda desenvolvida na linguagem Python e utiliza a própria API da rede social para o seu manejo. Projeto ainda em desenvolvimento e, portanto, com diversas falhas de segurança e bugs.
