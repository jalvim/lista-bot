###########################################################
#                                                         #
#   IMPLEMENTAÇÃO SIMPLES DE BOT DE LISTA DE COMPRAS P/   #
#   REDE TELEGRAM.                                        #
#                                                         #
###########################################################

import json
import os
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler

def init_list ():
    '''Método de inicialização com lista primária'''

    # Verifica se arquivo "lista.json" existe e retorna de acordo
    if os.path.exists("./lista.json"):
        return json.load(open("./lista.json"))
    else:
        return {
            "leite": 2,
            "pão_integral": 1,
            "pão_de_leite": 1,
            "bolo": 1,
            "açúcar": 1,
            "arroz": 2,
            "café": 1,
            "suco_de_laranja": 2,
        }

def update_list ():
    '''Função de atualização de arquivo de lista de compras'''

    # Abre arquivo "lista.json" e atualiza o json com nova lista
    with open("./lista.json", "w") as file:
        json.dump(lista_de_compras, file)

    return

def start (updater, context):
    '''Comando de inicialização de Bot com instruções.'''

    # Formatação de mensagem de inicialização
    msg  = "Bot de lista de compras.\nInstruções:\n\n"
    msg += "/start -> Apresenta esta própria mensagem"
    msg += "/add item qtd -> Acrescenta ou atualiza \"item\" na lista com quantidade \"qtd\".\n"
    msg += "/view -> Impresão da lista atual.\n"
    msg += "/remove item -> Remove item da lista.\n"

    # Caso exista, acrescenta lista de compras ao fim da msg
    if lista_de_compras:
        msg += "\nLista de compras:\n\n"
        for item, qtd in lista_de_compras.items():
            msg += f"{item}: {qtd}\n"

    # Envia mensagem contendo a string "msg"
    context.bot.send_message(
        chat_id=updater.effective_chat.id,
        text=msg
    )
    return

def add_item (updater, context):
    '''Comando de inserção de ítem na lista.'''

    try:
        # Parseia argumentos do comando
        item, qtd = context.args
        item = item.lower()
    except:
        # Em caso de erro, envia mensagem "err_msg"
        err_msg  = "Erro: Comando não executado de forma correta.\n\n"
        err_msg += "Tente a seguinte forma:\n"
        err_msg += "/add item quantidade"
        context.bot.send_message(
            chat_id=updater.effective_chat.id,
            text=err_msg
        )
        return

    # Envia mensagem de acordo com o preenchimento da lista
    if not lista_de_compras or item not in lista_de_compras.keys():
        lista_de_compras.update({item: qtd})
        msg=f"Item {item} adicionado com qtd {qtd}!"
    else:
        lista_de_compras[item] = qtd
        msg=f"Item {item} atualizado com qtd {qtd}!"

    context.bot.send_message(
        chat_id=updater.effective_chat.id,
        text=msg
    )

    # Atualiza o arquivo "lista.json" com lista atualizada
    update_list()
    return

def view (updater, context):
    '''Função de impressão de lista de compras.'''

    # Verifica se a lista está ou não vazia
    if not lista_de_compras:
        context.bot.send_message(
            chat_id=updater.effective_chat.id,
            text="Lista vazia!"
        )
        return

    # Criação dinâmica da string de lista com compras
    msg = "Lista de compras:\n\n"
    for item, qtd in lista_de_compras.items():
        msg += f"{item}: {qtd}\n"

    # Envia mensagem com texto da string "msg"
    context.bot.send_message(
        chat_id=updater.effective_chat.id,
        text=msg
    )
    return

def remove (updater, context):
    '''Função de remoção de item da lista de compras.'''

    try:
        # Parseia os argumentos do comando
        item = context.args[0]
        item = item.lower()
    except:
        # Em caso de erro, envia mensagem "err_msg"
        err_msg  = "Erro: Comando não executado de forma correta.\n\n"
        err_msg += "Tente a seguinte forma:\n"
        err_msg += "/remove item"
        context.bot.send_message(
            chat_id=updater.effective_chat.id,
            text=err_msg
        )
        return

    # Verifica se ítem está ou não na lista e, se sim, retira o elemento
    if lista_de_compras and item in lista_de_compras.keys():
        lista_de_compras.pop(item)
        msg=f"Item {item} removido da lista!"
    else:
        msg=f"Item {item} não encontrado na lista!"

    context.bot.send_message(
        chat_id=updater.effective_chat.id,
        text=msg
    )

    # Atualiza o arquivo "lista.json" com lista atualizada
    update_list()
    return

def unknown (updater, context):
    '''Função de exibição de comando não encontrado.'''

    context.bot.send_message(
        chat_id=updater.effective_chat.id,
        text="Desculpa, não entendi o seu comando..."
    )
    return

def main ():
    '''Função de execução principal do bot.'''

    # Manejo de token (id) do bot na rede Telegram
    TOKEN = ""
    with open("./TOKEN", "r") as file:
        TOKEN = file.read().replace("\n", "")

    # Cria objeto de interação do bot
    updt = Updater(token=TOKEN, use_context=True)
    disp = updt.dispatcher

    # Declaração de handlers
    start_handler = CommandHandler("start", start)
    view_handler = CommandHandler("view", view)
    add_handler = CommandHandler("add", add_item)
    remove_handler = CommandHandler("remove", remove)
    unknown_handler = MessageHandler(telegram.ext.Filters.command, unknown)

    # Adição de handlers em dispatcher do bot
    disp.add_handler(start_handler)
    disp.add_handler(view_handler)
    disp.add_handler(add_handler)
    disp.add_handler(remove_handler)
    disp.add_handler(unknown_handler)

    # Manejo do bot em si -> Loop infinito
    updt.start_polling()
    updt.idle()
    return

if __name__ == "__main__":
    print("Aplicação iniciada!")
    lista_de_compras = init_list()
    main()
